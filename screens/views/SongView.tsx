import React, { useCallback, useContext, useEffect, useState } from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import { useFocusEffect, useNavigation, useRoute } from '@react-navigation/native';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';

// DB
import { db, Lyric, Section } from '../../db';

// CONTEXTS
import { ModalContext } from '../../contexts';

// COMPONENTS
import { LyricComponent, SectionComponent } from '../../components';

// STYLES
import { DefaultStyles, GlobalStyles } from '../../styles';

// TYPES
import { LyricType, ModalContextType, SectionType, SongType } from '../../types';

// HOOKS
import { useLokiCollection } from '../../hooks';
import { CreateSectionComponent } from '../../components/modals/specifics';

export default function SongView() {

  const { setModalVisible, needRefresh, setNeedRefresh, dataToRefresh, setDataToRefresh } = useContext(ModalContext) as ModalContextType;

  const route = useRoute<any>();

  const { params } = route;

  const navigation: any = useNavigation();

  const { updateOne, loadData } = useLokiCollection();

  const [song, setSong] = useState<SongType>(params);
  const [lyrics, setLyrics] = useState<LyricType[]>([]);

  const [fav, setFav] = useState<boolean>(song.fav);

  const changeFavSong = async () => {
    const updatedSong: SongType = { ...song }
    updatedSong.fav = !fav;

    await updateOne('songs', updatedSong)
    setFav(!fav);
  };

  const addNote = () => {
    const newLyric: LyricType = new Lyric({ songId: song.$loki })
    navigation.navigate("LyricView", newLyric)
  }

  const loadLyrics = () => {
    if (db) {
      db.loadDatabase({}, () => {
        const resp: any = loadData('lyrics', { query: { songId: song.$loki } })
        setLyrics(resp);
      });
    }
  }

  const addSection = async (sectionName: string) => {
    const updatedSong: SongType = { ...song }
    const newSection: SectionType = new Section(song.sections.length, sectionName)
    
    updatedSong.sections = [...song.sections, newSection]

    await updateOne('songs', updatedSong)
    setSong(updatedSong)
  }

  useFocusEffect(
    useCallback(() => {
      loadLyrics();
    }, [])
  );

  return (
    <View style={[GlobalStyles.container, GlobalStyles.paddingXLg, GlobalStyles.paddingYmd]}>

      <View style={[GlobalStyles.row, GlobalStyles.alignCenter, GlobalStyles.justifyBetween, GlobalStyles.marginBottomLg]}>

        <Text style={[GlobalStyles.title]}>{song.title}</Text>

        <View style={[GlobalStyles.row, GlobalStyles.alignCenter, GlobalStyles.justifyBetween]}>

          <TouchableOpacity
            style={[
              GlobalStyles.roundedCircle,
              GlobalStyles.bgGrey,
              GlobalStyles.marginRightXs,
              GlobalStyles.paddingYxs,
              { paddingHorizontal: 10 },
            ]}
            onPress={changeFavSong}
          >
            <MaterialCommunityIcons
              name={fav ? 'star' : 'star-outline'}
              size={18}
              color={fav ? DefaultStyles.secondary : DefaultStyles.black}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              GlobalStyles.roundedCircle,
              GlobalStyles.bgGrey,
              GlobalStyles.marginRightXs,
              GlobalStyles.paddingYxs,
              { paddingHorizontal: 10 },
            ]}
            onPress={addNote}
          >
            <MaterialIcons
              name="note-add"
              size={18}
              color={DefaultStyles.black}
            />
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              GlobalStyles.roundedCircle,
              GlobalStyles.bgGrey,
              GlobalStyles.paddingYxs,
              { paddingHorizontal: 10 },
            ]}
            onPress={() => setModalVisible(<CreateSectionComponent createSection={addSection} />)}
          >
            <MaterialIcons
              name="my-library-add"
              size={18}
              color={DefaultStyles.black}
            />
          </TouchableOpacity>

        </View>

      </View>

      {
        lyrics.map((lyric) => (
          <LyricComponent lyric={lyric} setLyrirc={setLyrics} key={`SongView${lyric.$loki}`} />
        ))
      }

      {
        song.sections.map((section) => (
          <SectionComponent section={section} setSongs={setSong} key={`SongViewSections${section.sectionId}`} />
        ))
      }

      <Text style={[GlobalStyles.subTitleBold, GlobalStyles.marginYmd]}>Pistas sueltas</Text>

    </View>
  );
}
