import React, { useContext, useEffect, useState } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { MaterialCommunityIcons, MaterialIcons, FontAwesome5 } from '@expo/vector-icons';

// COMPONENTS
import { RecorderButton, TrackComponent } from '../../components';

// HOOKS
import { useNameFormatter } from '../../hooks';

// STYLES
import { DefaultStyles, GlobalStyles } from '../../styles';
import { ListOfCategoriesType, ModalContextType, TrackType } from '../../types';
import { ModalContext } from '../../contexts';

const CategoryView = ({ route }: {route: any}) => {

	const [trackList, setTrackList] = useState<TrackType[]>([]);

	const { capitalice } = useNameFormatter();

	const { name, icon, category, materialIcons, fontAwesome }: ListOfCategoriesType = route.params;

	const pushNewTrck = (track: TrackType) => {
		setTrackList([...trackList, track]);
	};

	useEffect(() => {
	}, []);

	return (
		<View style={[GlobalStyles.bgWhite, GlobalStyles.container, GlobalStyles.paddingMd, GlobalStyles.paddingBottomXl]}>
			<View
				style={[
					GlobalStyles.row,
					GlobalStyles.alignCenter,
					GlobalStyles.marginBottomSm,
					GlobalStyles.paddingBottomXl,
				]}
			>
				<View style={[GlobalStyles.roundedCircle, GlobalStyles.bgGrey, GlobalStyles.paddingXs]}>
					{materialIcons ? (
						<MaterialIcons name={icon as any} color={DefaultStyles.primary} size={20} />
					) : fontAwesome ? (
						<FontAwesome5 name={icon} color={DefaultStyles.primary} size={20} />
					) : (
						<MaterialCommunityIcons name={icon as any} color={DefaultStyles.primary} size={20} />
					)}
				</View>

				<Text style={[GlobalStyles.title, GlobalStyles.marginLeftXs]}>{capitalice(name)}</Text>
			</View>

			{trackList.length ? (
				<ScrollView>
					{trackList.map((track, i) => (
						<TrackComponent track={track} key={`CategoryView${track.$loki}`} />
					))}
				</ScrollView>
			) : (
				<View style={[GlobalStyles.container, GlobalStyles.centered, GlobalStyles.marginBottomXl]}>
					<MaterialIcons name="play-disabled" size={120} color={DefaultStyles.darkGrey} />
					<Text style={[GlobalStyles.textGrey, GlobalStyles.label, GlobalStyles.marginTopSm]}>
						Todavia no tenes {name} grabados
					</Text>
				</View>
			)}

			<RecorderButton category={category} />
		</View>
	);
};

export default CategoryView;
