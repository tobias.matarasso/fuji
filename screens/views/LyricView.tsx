import React, { useCallback, useEffect, useState } from 'react';
import { TextInput, TouchableOpacity, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import * as Clipboard from 'expo-clipboard';
import { Lyric } from '../../db';

// STYLES
import { DefaultStyles, GlobalStyles } from '../../styles';

// TYPES
import { LyricType } from '../../types';

// HOOKS
import { useLokiCollection } from '../../hooks';
import { useFocusEffect, useNavigation, useRoute } from '@react-navigation/native';

export default function LyricView() {

  const route = useRoute<any>();

  const { params } = route;

  const [lyric, setLyric] = useState<LyricType>(params || {});

  const [textSize, setTextSize] = useState(18);
  const [copied, setCopied] = useState(false);

  const navigation: any = useNavigation();

  const { insertOne, updateOne } = useLokiCollection()

  const saveFontSize = async () => {
    try {
      await AsyncStorage.setItem('fontSize', textSize.toString());
    } catch (error) {
      console.error('Error al guardar el tamaño de la fuente', error);
    }
  };

  const loadFontSize = async () => {
    try {
      const storedFontSize = await AsyncStorage.getItem('fontSize');
      if (storedFontSize !== null) {
        setTextSize(parseInt(storedFontSize, 10));
      }
    } catch (error) {
      console.error('Error al cargar el tamaño de la fuente', error);
    }
  };

  const saveSong = async () => {
    if (!lyric.$loki && (lyric?.title || lyric?.lyrics)) {
      const newLyric: LyricType = await insertOne('lyrics', new Lyric(lyric))
      
      setLyric(newLyric);
      
    } else if (lyric.$loki) {
      await updateOne('lyrics', lyric)
      return null
    }

  }

  const copyToClipboard = async () => {
    setCopied(true);
    if (lyric.lyrics) await Clipboard.setStringAsync(lyric.lyrics);
    setTimeout(() => setCopied(false), 3000);
  };
  
  // Guardar la canción antes de salir de la pantalla
  const onBeforeRemove = async (e: any) => {
    e.preventDefault();
    await saveSong()
    navigation.dispatch(e.data.action);
  };

  useEffect(() => {
    loadFontSize();
  }, []);

  // Cambiar el tamaño de la fuente
  useEffect(() => {
    saveFontSize();
  }, [textSize]);
  
  // Guardar la canción cuando detecta cambios
  useEffect(() => {
    const handler = setTimeout(() => {
      saveSong();
    }, 600);

    return () => {
      clearTimeout(handler);
    }
  }, [lyric]);
  
  // Wait to save the song before leaving the screen
  useFocusEffect(
    useCallback(() => {

      navigation.addListener('beforeRemove', onBeforeRemove);

      return () => {
        navigation.removeListener('beforeRemove', onBeforeRemove);
      };
    }, [lyric, navigation])
  );

  return (
    <View style={[GlobalStyles.container, GlobalStyles.paddingXLg, GlobalStyles.paddingYmd]}>

      <View style={[GlobalStyles.row, GlobalStyles.alignCenter, GlobalStyles.justifyBetween, GlobalStyles.marginBottomSm]}>

        <TextInput
          style={[GlobalStyles.title]}
          placeholder="Título"
          value={lyric.title}
          onChangeText={(text) => setLyric({ ...lyric, title: text })}
        />

        <View style={[GlobalStyles.row, GlobalStyles.alignCenter, GlobalStyles.justifyBetween]}>

          <TouchableOpacity
            style={[
              GlobalStyles.roundedCircle,
              GlobalStyles.bgGrey,
              GlobalStyles.marginRightMd,
              GlobalStyles.paddingYxxs,
              { paddingHorizontal: 5 },
            ]}
            onPress={copyToClipboard}
          >
            <MaterialCommunityIcons name="content-copy" size={18} color={copied ? DefaultStyles.secondary :DefaultStyles.darkGrey} />
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              GlobalStyles.roundedCircle,
              GlobalStyles.bgGrey,
              GlobalStyles.marginRightXs,
              GlobalStyles.paddingYxxs,
              { paddingHorizontal: 5 },
            ]}
            onPress={() => setTextSize(textSize - 1)}
          >
            <MaterialCommunityIcons name="format-font-size-decrease" size={18} color={DefaultStyles.darkGrey} />
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              GlobalStyles.roundedCircle,
              GlobalStyles.bgGrey,
              GlobalStyles.paddingYxxs,
              { paddingHorizontal: 5 },
            ]}
            onPress={() => setTextSize(textSize + 1)}
          >
            <MaterialCommunityIcons name="format-font-size-increase" size={18} color={DefaultStyles.darkGrey} />
          </TouchableOpacity>
        </View>

      </View>

      <TextInput
        style={[GlobalStyles.heading, GlobalStyles.container, { lineHeight: (textSize * 1.18), fontSize: textSize }]}
        placeholder="Letra"
        value={lyric.lyrics}
        onChangeText={(text) => setLyric({ ...lyric, lyrics: text })}
        multiline
        textAlignVertical="top"
      />
    </View>
  );
}
