// MAIN
import HomeScreen from './main/HomeScreen';
import SongsScreen from './main/SongsScreen';
import CategoryScreen from './main/CategoryScreen';
import LyricScreen from './main/LyricScreen';

// // VIEWS
import CategoryView from './views/CategoryView';
// import SaveRecordView from './views/SaveRecordView';
import SongView from './views/SongView';
import LyricView from './views/LyricView';

export {
	HomeScreen,
	SongsScreen,
	CategoryScreen,
	LyricScreen,
	
	CategoryView,
	// SaveRecordView,
	SongView,
	LyricView
};
