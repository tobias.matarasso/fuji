import React from 'react';
import { Image, View } from 'react-native';

export default function SplashScreen() {
	return (
		<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
			<Image source={require('../assets/splash.png')} style={{ maxWidth: '100%' }} />
		</View>
	);
}
