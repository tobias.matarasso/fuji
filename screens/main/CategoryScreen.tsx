import React from 'react';
import { View, FlatList, Dimensions, Text, TouchableOpacity, ListRenderItem } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialCommunityIcons, MaterialIcons, FontAwesome5 } from '@expo/vector-icons';

// HOOKS
import { useGetIconName } from '../../hooks';

// STYLES
import { DefaultStyles, GlobalStyles } from '../../styles';

// TYPES
import { CategoriesNamesType, CategoriesType, ListOfCategoriesType } from '../../types';

const numColumns = 2;
const size = Dimensions.get('window').width / numColumns - 30;

const CategoryScreen = ({ navigation }: {navigation: any}) => {
	const { getIconName } = useGetIconName();

	const categories: ListOfCategoriesType[] = [
		{
			name: 'Riffs',
		},
		{
			name: 'Canciones',
		},
		{
			name: 'Ideas',
		},
		{
			name: 'Notas de voz',
			materialIcons: true,
		},
		{
			name: 'Ritmos',
			fontAwesome: true,
		},
		{
			name: 'Lineas de bajo',
		},
	];

	const formatCategory = (cat: CategoriesNamesType) => {
		return cat?.toLowerCase().replace(' ', '_') as CategoriesType;
	};

	const renderItem: ListRenderItem<ListOfCategoriesType> = ({item}) => (
		<TouchableOpacity
			onPress={() =>
				navigation.navigate('CategoryView', {
					...item,
					category: formatCategory(item.name),
					icon: getIconName(formatCategory(item.name)) as any,
				})
			}
		>
			<LinearGradient
				colors={['#B981CD', '#FFB47D']}
				start={{ x: 0.3, y: 1 }}
				end={{ x: 1, y: 0 }}
				style={[
					GlobalStyles.rounded,
					GlobalStyles.centered,
					GlobalStyles.marginSm,
					{
						width: size,
						height: size,
					},
				]}
			>
				<View style={[GlobalStyles.alignCenter]}>
					{item.materialIcons ? (
						<MaterialIcons
							name={getIconName(formatCategory(item.name)) as any}
							size={60}
							color={DefaultStyles.white}
						/>
					) : item.fontAwesome ? (
						<FontAwesome5
							name={getIconName(formatCategory(item.name)) as any}
							size={60}
							color={DefaultStyles.white}
						/>
					) : (
						<MaterialCommunityIcons
							name={getIconName(formatCategory(item.name)) as any}
							size={60}
							color={DefaultStyles.white}
						/>
					)}
					<Text style={[GlobalStyles.heading, GlobalStyles.marginTopSm, GlobalStyles.textWhite]}>
						{item.name}
					</Text>
				</View>
			</LinearGradient>
		</TouchableOpacity>
	);

	return (
		<View style={[GlobalStyles.centered, GlobalStyles.bgWhite, { flex: 1 }]}>
			<View style={{ flex: 1, justifyContent: 'center' }}>
				<FlatList
					contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
					data={categories}
					renderItem={renderItem}
					keyExtractor={(item, index) => index.toString()}
					numColumns={numColumns}
				/>
			</View>
		</View>
	);
};

export default CategoryScreen;
