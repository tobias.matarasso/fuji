import React, { useContext, useEffect, useState } from 'react';
import { ScrollView, View } from 'react-native';
import { db } from '../../db';

// CONTEXTS
import { ModalContext } from '../../contexts';

// STYLES
import { GlobalStyles } from '../../styles';

// COMPONENTS
import { SongComponent, Button } from '../../components';

// TYPES
import { ModalContextType, SongType } from '../../types';

// HOOKS
import { useLokiCollection } from '../../hooks';

const SongsScreen = () => {
	const { needRefresh, setNeedRefresh } = useContext(ModalContext) as ModalContextType;

	const { loadData } = useLokiCollection()

	const [songs, setSongs] = useState<SongType[]>([]);
	const [filter, setFilter] = useState<'all' | boolean>('all');

	useEffect(() => {
		if (db) {
			db.loadDatabase({}, () => {
				const resp: any = loadData('songs')
				setSongs(resp);
			});
		}
	}, []);

	useEffect(() => {
		if (db && needRefresh) {
			const resp: any = loadData('songs')
			setSongs(resp);
			setNeedRefresh(false)
		}
	}, [needRefresh]);

	return (
		<View style={[GlobalStyles.container, GlobalStyles.bgWhite, GlobalStyles.paddingTopLg, GlobalStyles.paddingXLg]}>
			<View style={[GlobalStyles.row, GlobalStyles.alignCenter, GlobalStyles.justifyBetween, GlobalStyles.paddingBottomLg]}>
				<View style={[{width: '33%'}, GlobalStyles.paddingRightXs]}>
					<Button action={() => setFilter('all')} padding={'small'} textStyle='paragraph' title="Todo" rounded={true} textColor={filter == 'all' ? 'white' : 'grey'} color={filter == 'all' ? 'gradient' : 'grey'} />
				</View>

				<View style={[{width: '33%'}, GlobalStyles.paddingXXs]}>
					<Button action={() => setFilter(true)} padding={'small'} textStyle='paragraph' title="Favoritas" rounded={true} textColor={filter == true ? 'white' : 'grey'} color={filter == true ? 'gradient' : 'grey'} />
				</View>

				<View style={[{width: '33%'}, GlobalStyles.paddingLeftXs]}>
					<Button action={() => setFilter(false)} padding={'small'} textStyle='paragraph' title="Normales" rounded={true} textColor={filter == false ? 'white' : 'grey'} color={filter == false ? 'gradient' : 'grey'} />
				</View>
			</View>
			<ScrollView>
				{(filter != 'all' ? songs.filter((songs: SongType) => songs.fav == filter as boolean) : songs).map((song) => (
					<SongComponent song={song} setSongs={setSongs} key={`SongCompoentnsSongs${song.$loki}`} />
				))}
			</ScrollView>
		</View>
	);
};

export default SongsScreen;
