import React, { useCallback, useContext, useEffect, useState } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { db } from '../../db';

// CONTEXTS
import { ModalContext } from '../../contexts';

// STYLES
import { DefaultStyles, GlobalStyles } from '../../styles';

// TYPES
import { LyricType, ModalContextType } from '../../types';

// HOOKS
import { useLokiCollection } from '../../hooks';
import { useFocusEffect } from '@react-navigation/native';
import { LyricComponent } from '../../components';

const LyricScreen = () => {

	const { loadData } = useLokiCollection()

	const [lyrics, setLyrics] = useState<LyricType[]>([]);

	const loadLyrics = () => {
		if (db) {
			db.loadDatabase({}, () => {
				const resp: any = loadData('lyrics')
				setLyrics(resp);
			});
		}
	}

	useFocusEffect(
		useCallback(() => {
			loadLyrics();
		}, [])
	);

	return (
		<ScrollView style={[GlobalStyles.bgWhite, GlobalStyles.container, GlobalStyles.paddingTopLg, GlobalStyles.paddingXLg]}>
			{lyrics.length ? (
				lyrics.map((lyric) => (
					<LyricComponent lyric={lyric} setLyrirc={setLyrics} key={`LyricScreen${lyric.$loki}`}/>
				))
			) : (
				<View style={[GlobalStyles.w100, GlobalStyles.centered, GlobalStyles.container]}>
					<MaterialIcons name="speaker-notes-off" size={120} color={DefaultStyles.darkGrey} />
					<Text style={[GlobalStyles.textGrey, GlobalStyles.label, GlobalStyles.marginTopSm]}>
						No hay notas guardadas
					</Text>
				</View>
			)}
		</ScrollView>
	);
};

export default LyricScreen;
