import { useCallback, useContext, useEffect, useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';

// DB
import { db, Song } from '../../db';

// CONTETXS
import { ModalContext } from '../../contexts';

// COMPONENTS
import { CreateSongComponent, SongComponent } from '../../components';

// HOOKS
import { useLokiCollection } from '../../hooks';

// STYLES
import { DefaultStyles, GlobalStyles } from '../../styles';

// TYPES
import { ModalContextType, SongType } from '../../types';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

export default function HomeScreen() {

  const { setModalVisible, needRefresh, setNeedRefresh } = useContext(ModalContext) as ModalContextType;

  const [songs, setSongs] = useState<SongType[]>([]);

  const { loadData } = useLokiCollection();

  const navigation: any = useNavigation();

  const shorcuts = [
    {
      name: 'New song',
      icon: 'plus',
      onPress: () => {
        setModalVisible(<CreateSongComponent />);
      },
    },
    {
      name: 'Write lyric',
      icon: 'message-plus',
      onPress: () => {
        navigation.navigate("LyricView");
      },
    },
    {
      name: 'Add secction',
      icon: 'my-library-add',
      materialIcon: true,
      onPress: () => {
        console.log('New sectioin');
      },
    },
  ];

  const loadSongs = () => {
    if (db) {
      db.loadDatabase({}, () => {
        const resp: any = loadData('songs', { limit: 3, query: { fav: true } });
        setSongs(resp);
      });
    }
  }

  useEffect(() => {
    if (needRefresh) {
      loadSongs()
      setNeedRefresh(false)
    }
  }, [needRefresh]);

  useFocusEffect(
    useCallback(() => {
      loadSongs()
    }, [])
  );

  return (
    <View style={[GlobalStyles.alignCenter, GlobalStyles.bgWhite, GlobalStyles.paddingMd, GlobalStyles.container]}>

      <View style={[GlobalStyles.w100]}>
        {songs.map((song: SongType, i) => (
          <SongComponent song={song} setSongs={setSongs} key={`HomeCompoentnsSongs${song.$loki}`} />
        ))}
      </View>


      <View style={[GlobalStyles.row, GlobalStyles.w100, GlobalStyles.marginYsm, GlobalStyles.justifyBetween]}>

        {shorcuts.map((shortcut, i) => (
          <TouchableOpacity
            key={shortcut.name + i}
            style={[GlobalStyles.alignCenter]}
            onPress={shortcut.onPress}
          >
            <View
              style={[
                GlobalStyles.bgGrey,
                GlobalStyles.paddingSm,
                GlobalStyles.roundedCircle,
                { borderRadius: 50 },
                GlobalStyles.marginBottomXs,
              ]}
            >
              {shortcut.materialIcon ? (
                <MaterialIcons name={shortcut.icon as any} size={24} color={DefaultStyles.black} />
              ) : (
                <MaterialCommunityIcons
                  name={shortcut.icon as any}
                  size={24}
                  color={DefaultStyles.black}
                />
              )}
            </View>
            <Text style={[GlobalStyles.textCenter]}>{shortcut.name}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
}