import React, { useContext, useState } from 'react';
import { View, Text } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

// DB
import { db, Section, Song } from '../../../db';

// CONTEXTS
import { ModalContext } from '../../../contexts';

// COMPONENTS
import { Button, InputField } from '../../globals';

// HOOKS
import { useNameFormatter } from '../../../hooks';

// STYLES
import { DefaultStyles, GlobalStyles } from '../../../styles';

// TYPES
import { ModalContextType } from '../../../types';

function CreateSectionComponent({createSection}: {createSection: (section: string) => void}) {
	const { setModalVisible } = useContext(ModalContext) as ModalContextType;
	
	const [sectionName, setSectionName] = useState<string>('');
	const [isCreating, setIsCreating] = useState(false);

	const { capitalice } = useNameFormatter();

	const createSong = async () => {
		createSection(sectionName);
		setIsCreating(false);
		setModalVisible(false);
		setSectionName('');
	};

	return (
		<View>
			<Text style={[GlobalStyles.subTitleBold, GlobalStyles.textCenter, GlobalStyles.paddingBottomMd]}>
				Crear sección
			</Text>

			<View
				style={[
					GlobalStyles.bgGrey,
					GlobalStyles.roundedSm,
					GlobalStyles.paddingXxs,
					GlobalStyles.paddingYxxs,
					GlobalStyles.marginBottomMd,
					GlobalStyles.alignCenter,
					GlobalStyles.row,
				]}
			>
				<MaterialIcons name="my-library-books" size={20} color={DefaultStyles.primary} />
				<InputField
					customStyles={[GlobalStyles.marginLeftXs, GlobalStyles.container]}
					placeholder="Titulo"
					background="grey"
					value={capitalice(sectionName)}
					handleChange={setSectionName}
					focus={true}
				/>
			</View>

			<View style={[GlobalStyles.row, GlobalStyles.w100, GlobalStyles.justifyBetween]}>
				<Button
					title="Cancelar"
					textStyle="paragraph"
					textColor="danger"
					color="none"
					disabled={isCreating}
					action={() => setModalVisible(false)}
				/>
				<Button
					title="Crear"
					textStyle="paragraph"
					color="gradient"
					loading={isCreating}
					disabled={isCreating}
					action={createSong}
				/>
			</View>
		</View>
	);
}

export default CreateSectionComponent;
