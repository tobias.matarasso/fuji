import React, { useContext, useState } from 'react';
import { View, Text } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

// DB
import { db, Song } from '../../../db';

// CONTEXTS
import { ModalContext } from '../../../contexts';

// COMPONENTS
import { Button, InputField } from '../../globals';

// HOOKS
import { useLokiCollection, useNameFormatter } from '../../../hooks';

// STYLES
import { DefaultStyles, GlobalStyles } from '../../../styles';

// TYPES
import { ModalContextType, SongType } from '../../../types';

function CreateSongComponent() {
	const { setModalVisible, setNeedRefresh } = useContext(ModalContext) as ModalContextType;

	const [songName, setSongName] = useState<string>('');
	const [isCreating, setIsCreating] = useState(false);

	const { capitalice } = useNameFormatter();

	const { insertOne } = useLokiCollection()

	const createSong = async () => {
		const newSong = new Song(songName);

		insertOne('songs', newSong)

		setSongName('');
		setNeedRefresh(true);
		setIsCreating(false);
		setModalVisible(false);
	};

	return (
		<View>
			<Text style={[GlobalStyles.subTitleBold, GlobalStyles.textCenter, GlobalStyles.paddingBottomMd]}>
				Crear canción
			</Text>

			<View
				style={[
					GlobalStyles.bgGrey,
					GlobalStyles.roundedSm,
					GlobalStyles.paddingXxs,
					GlobalStyles.paddingYxxs,
					GlobalStyles.marginBottomMd,
					GlobalStyles.alignCenter,
					GlobalStyles.row,
				]}
			>
				<MaterialIcons name="my-library-music" size={20} color={DefaultStyles.primary} />
				<InputField
					customStyles={[GlobalStyles.marginLeftXs, GlobalStyles.container]}
					placeholder="Titulo"
					background="grey"
					value={capitalice(songName)}
					handleChange={setSongName}
					focus={true}
				/>
			</View>

			<View style={[GlobalStyles.row, GlobalStyles.w100, GlobalStyles.justifyBetween]}>
				<Button
					title="Cancelar"
					textStyle="paragraph"
					textColor="danger"
					color="none"
					disabled={isCreating}
					action={() => setModalVisible(false)}
				/>
				<View>
					<Button
						title="Crear"
						textStyle="paragraph"
						color="gradient"
						loading={isCreating}
						disabled={isCreating}
						action={createSong}
					/>
				</View>
			</View>
		</View>
	);
}

export default CreateSongComponent;
