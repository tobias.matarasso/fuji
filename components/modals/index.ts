import GeneralStructureModal from './GeneralStructureModal';

// SPECIFICS
import { CreateSongComponent } from './specifics';

export { GeneralStructureModal, CreateSongComponent };
