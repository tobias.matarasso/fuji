import React, { useContext } from 'react'
import { View, Modal } from 'react-native';

// CONTEXTS
import { ModalContext } from '../../contexts';

// STYLES
import { GlobalStyles } from '../../styles';

// TYPES
import { ModalContextType } from '../../types';

function GeneralStructure() {

	const { modalVisible } = useContext(ModalContext) as ModalContextType;

    return (
        <View style={[GlobalStyles.centered]}>
            <Modal
                animationType="fade"
                transparent={true}
                visible={!!modalVisible} // Arrives a component or a false
                onRequestClose={() => {
                    alert('Modal has been closed.');
                }}>
                <View style={[ GlobalStyles.container, GlobalStyles.bgFaded, GlobalStyles.centered, GlobalStyles.paddingLg]}>
                    <View style={[GlobalStyles.bgWhite, GlobalStyles.paddingMd, GlobalStyles.rounded]}>
                        {modalVisible}
                    </View>
                </View>
            </Modal>
        </View>
    )
}

export default GeneralStructure