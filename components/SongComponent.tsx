import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Animated, Pressable } from 'react-native';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { Swipeable } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

// STYLES
import { GlobalStyles, DefaultStyles } from '../styles';

// TYPES
import { SongType } from '../types';

// HOOKS
import { useLokiCollection } from '../hooks';

const SongComponent = ({ song, setSongs }: { song: SongType, setSongs?: any}) => {
	const [fav, setFav] = useState<boolean>(song.fav);

	const { updateOne, deleteOne } = useLokiCollection()

	const navigation: any = useNavigation();

	const changeFavSong = async () => {
		const updatedSong: SongType = { ...song }
		updatedSong.fav = !fav;
		await updateOne('songs', updatedSong)
		setFav(!fav);
	};

	const handleDeleteSong = async () => {
		await deleteOne('songs', song.$loki as number);
		setSongs((prev: SongType[]) => prev.filter((s: SongType) => s.$loki !== song.$loki));
	};

	const renderRightActions = (progress: any, dragX: any) => {
		const scale = dragX.interpolate({
			inputRange: [-100, 0],
			outputRange: [2, 0],
			extrapolate: 'clamp',
		});
		return (
			<TouchableOpacity onPress={handleDeleteSong} activeOpacity={0.6} style={{ width: 60 }}>
				<View style={[GlobalStyles.row, GlobalStyles.centered, GlobalStyles.container, GlobalStyles.bgDanger]}>
					<Animated.Text style={{ transform: [{ scale }] }}>
						<MaterialIcons name='delete' size={25} color="white" />
					</Animated.Text>
				</View>
			</TouchableOpacity>
		);
	};

	return (
		<Swipeable containerStyle={[GlobalStyles.marginBottomSm, GlobalStyles.bgDanger, GlobalStyles.rounded]} renderRightActions={renderRightActions}>

			<Pressable
				style={({ pressed }) => [
					GlobalStyles.row,
					GlobalStyles.alignCenter,
					GlobalStyles.justifyBetween,
					GlobalStyles.paddingXs,
					!pressed ? GlobalStyles.bgGrey : GlobalStyles.bgLightGrey,
				]}
				onPress={() => navigation.navigate('SongView', song)}
			>
				<View style={[GlobalStyles.row, GlobalStyles.alignCenter]}>
					<View
						style={[
							GlobalStyles.row,
							GlobalStyles.centered,
							GlobalStyles.bgWhite,
							GlobalStyles.rounded,
							GlobalStyles.paddingSm,
						]}
					>
						<MaterialIcons name="my-library-music" color={DefaultStyles.primary} size={18} />
					</View>
					<Text style={[GlobalStyles.subTitleBold, GlobalStyles.marginLeftXs]}>{song.title}</Text>
				</View>

				<View style={[GlobalStyles.row, GlobalStyles.paddingRightXs]}>
					<View style={GlobalStyles.marginRightXs}>
						<MaterialCommunityIcons name="play" size={25} color={DefaultStyles.black} />
						<View style={GlobalStyles.badge}>
							<Text style={GlobalStyles.burgetCount}>{song.tracks}</Text>
						</View>
					</View>
					<View style={GlobalStyles.marginRightXs}>
						<MaterialIcons name="speaker-notes" size={25} color={DefaultStyles.black} />
						<View style={GlobalStyles.badge}>
							<Text style={GlobalStyles.burgetCount}>{song.lyrics}</Text>
						</View>
					</View>
					<TouchableOpacity onPress={changeFavSong} style={GlobalStyles.marginRightXs}>
						<MaterialCommunityIcons
							name={fav ? 'star' : 'star-outline'}
							size={25}
							color={fav ? DefaultStyles.secondary : DefaultStyles.black}
						/>
					</TouchableOpacity>
				</View>
			</Pressable>

		</Swipeable>
	);
};

export default SongComponent;
