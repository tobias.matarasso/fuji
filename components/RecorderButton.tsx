import React, { useState } from 'react'
import { View, TouchableOpacity } from 'react-native'
import { useNavigation } from '@react-navigation/native';

// ICONS
import { MaterialIcons } from '@expo/vector-icons'

// STYLES
import { GlobalStyles, DefaultStyles } from '../styles'

const RecorderButton = ({ category }: {category: any}) => {

    const [recording, setRecording] = useState();
    const [isPaused, setIsPaused] = useState(false);

    const navigation: any = useNavigation();

    return (
        <View
            style={[
                GlobalStyles.w100,
                GlobalStyles.alignCenter,
                GlobalStyles.justifyAround,
                GlobalStyles.absolute,
                GlobalStyles.marginXmd,
                GlobalStyles.row,
                { bottom: 25 }
            ]}
        >

            {recording &&
                <TouchableOpacity
                    // onPress={cancelRecording}
                >
                    <MaterialIcons name="cancel" size={20} color={DefaultStyles.black} />
                </TouchableOpacity>
            }

            <TouchableOpacity
                style={[
                    GlobalStyles.roundedCircle,
                    GlobalStyles.bgWhite,
                    GlobalStyles.paddingSm,
                    GlobalStyles.shadow,
                ]}
                // onPress={recording ? stopRecording : startRecording}
            >
                <MaterialIcons name="mic" color={recording ? DefaultStyles.secondary : DefaultStyles.primary} size={40} />
            </TouchableOpacity>

            {recording && (
                <TouchableOpacity
                    // onPress={isPaused ? resumeRecording : pauseRecording}
                >
                    <MaterialIcons name={isPaused ? "play-arrow" : "pause"} size={20} color={DefaultStyles.black} />
                </TouchableOpacity>)
            }

        </View>
    )
}

export default RecorderButton
