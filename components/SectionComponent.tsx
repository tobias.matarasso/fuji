import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Animated } from 'react-native';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { Swipeable } from 'react-native-gesture-handler';

// STYLES
import { GlobalStyles, DefaultStyles } from '../styles';

// TYPES
import { SectionType, SongType } from '../types';

// HOOKS
import { useLokiCollection } from '../hooks';

const SectionComponent = ({ section, setSongs }: { section: SectionType, setSongs?: any }) => {

	const { updateOne, loadData } = useLokiCollection();
	
	const navigation: any = useNavigation();

	const handleDeleteSong = async () => {

		let updatedSong: SongType | {} = {}

		setSongs((song: SongType) => {
			updatedSong = {...song, sections: song.sections.filter((s: SectionType) => s.sectionId !== section.sectionId)}
			return updatedSong
		});

		await updateOne('songs', updatedSong)
	};

	const renderRightActions = (progress: any, dragX: any) => {
		const scale = dragX.interpolate({
			inputRange: [-100, 0],
			outputRange: [2, 0],
			extrapolate: 'clamp',
		});
		return (
			<TouchableOpacity onPress={handleDeleteSong} activeOpacity={0.6} style={{ width: 60 }}>
				<View style={[GlobalStyles.row, GlobalStyles.centered, GlobalStyles.container, GlobalStyles.bgDanger]}>
					<Animated.Text style={{ transform: [{ scale }] }}>
						<MaterialIcons name='delete' size={25} color="white" />
					</Animated.Text>
				</View>
			</TouchableOpacity>
		);
	};

	return (
		<Swipeable containerStyle={[GlobalStyles.marginBottomSm, GlobalStyles.bgDanger, GlobalStyles.rounded]} renderRightActions={renderRightActions}>

			<TouchableOpacity
				style={[
					GlobalStyles.row,
					GlobalStyles.alignCenter,
					GlobalStyles.justifyBetween,
					GlobalStyles.paddingSm,
					GlobalStyles.bgGrey,
				]}
				onPress={() => navigation.navigate('SectionView', section)}
			>
				<View style={[GlobalStyles.row, GlobalStyles.alignCenter]}>

					<MaterialIcons name="my-library-books" color={DefaultStyles.black} size={40} />

					<Text style={[GlobalStyles.subTitleBold, GlobalStyles.marginLeftXs]}>{section.title}</Text>
				</View>

				<View style={[GlobalStyles.row]}>
					<View style={GlobalStyles.marginRightXs}>
						<MaterialCommunityIcons name="play" size={25} color={DefaultStyles.black} />
						<View style={GlobalStyles.badge}>
							<Text style={GlobalStyles.burgetCount}>{section.tracks}</Text>
						</View>
					</View>
				</View>
			</TouchableOpacity>

		</Swipeable>
	);
};

export default SectionComponent;
