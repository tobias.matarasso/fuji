import React, { useContext, useEffect, useState } from 'react';
import { useNavigation, useNavigationState } from '@react-navigation/native';
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';
import { TouchableOpacity } from 'react-native';

// CONTEXTS
import { ModalContext } from '../contexts';

// COMPONENTS

import { CreateSongComponent } from './modals';

// STYLES
import { DefaultStyles } from '../styles';

// TYPES
import { ModalContextType } from '../types';

function MicIcon() {
	const { setModalVisible } = useContext(ModalContext) as ModalContextType;
	const [isRecording, setIsRecording] = useState(false);

	const currentTab = useNavigationState((state) => state.routes[state.index].name);

	const navigation: any = useNavigation();

	const action = () => {
		switch (currentTab) {
			case 'HomeView':
			case 'Cats':
				setIsRecording(!isRecording);
				break;
			case 'Songs':
				setModalVisible(<CreateSongComponent />);
				break;
			case 'Lyrics':
				navigation.navigate("LyricView");
				break;
		}
	};

	const Icon = () => {
		if (currentTab === 'Lyrics' || currentTab === 'SearchView' || currentTab === 'Songs') {
			return <MaterialCommunityIcons name="plus" color={DefaultStyles.primary} size={40} />;
		}
		return (
			<MaterialIcons name="mic" color={!isRecording ? DefaultStyles.faded_bg : DefaultStyles.primary} size={40} />
		);
	};

	useEffect(() => {
		if (isRecording) {
			// navigation.navigate('SaveRecord');
		}
	}, [isRecording]);

	return (
		<TouchableOpacity onPress={action}>
			<Icon />
		</TouchableOpacity>
	);
}

export default MicIcon;
