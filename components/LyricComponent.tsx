import React from 'react';
import { View, Text, TouchableOpacity, Animated } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import { Swipeable } from 'react-native-gesture-handler';

// STYLES
import { GlobalStyles } from '../styles';

// TYPES
import { LyricType } from '../types';
import { useLokiCollection } from '../hooks';
import { useNavigation } from '@react-navigation/native';

const LyricComponent = ({ lyric, setLyrirc }: { lyric: LyricType, setLyrirc?: any }) => {

	const { deleteOne } = useLokiCollection()
	const navigation: any = useNavigation();

	const handleDeleteLyric = async () => {
		await deleteOne('lyrics', lyric.$loki as number);
		setLyrirc((prev: LyricType[]) => prev.filter((s: LyricType) => s.$loki !== lyric.$loki));
	};

	const renderRightActions = (progress: any, dragX: any) => {
		const scale = dragX.interpolate({
			inputRange: [-100, 0],
			outputRange: [2, 0],
			extrapolate: 'clamp',
		});
		return (
			<TouchableOpacity onPress={handleDeleteLyric} activeOpacity={0.6} style={{ width: 60 }}>
				<View style={[GlobalStyles.row, GlobalStyles.centered, GlobalStyles.container, GlobalStyles.bgDanger]}>
					<Animated.Text style={{ transform: [{ scale }] }}>
						<MaterialIcons name='delete' size={25} color="white" />
					</Animated.Text>
				</View>
			</TouchableOpacity>
		);
	};

	return (
		<Swipeable containerStyle={[GlobalStyles.marginBottomSm, GlobalStyles.bgDanger, GlobalStyles.rounded]} renderRightActions={renderRightActions}>

			<TouchableOpacity
				style={[
					GlobalStyles.paddingMd,
					GlobalStyles.bgGrey,
				]}
				onPress={() => navigation.navigate("LyricView", lyric)}
			>
				<Text style={[GlobalStyles.subTitleBold, GlobalStyles.marginBottomXs]}>{lyric.title}</Text>
				<Text style={[GlobalStyles.paragraph, {maxHeight: 34}]}>{lyric.lyrics}</Text>
			</TouchableOpacity>

		</Swipeable>
	);
};

export default LyricComponent;
