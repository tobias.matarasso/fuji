import React, { useEffect, useState, useRef } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

// STYLES
import { GlobalStyles, DefaultStyles } from '../styles';

// TYPES
import { TrackType } from '../types';

// COLLECTIONS

// HOOKS
import { useNameFormatter, useGetIconName } from '../hooks';

// COMPONENTS
import { InputField } from './globals';

const TrackComponent = ({ track }: { track: TrackType }) => {
	const [trackTitle, setTackTitle] = useState<TrackType['title']>(track.title);
	const [edit, setEdit] = useState<boolean>(false);
	const [sound, setSound] = useState();

	const { getIconName } = useGetIconName();
	const { formatDuration } = useNameFormatter();

	const inputRef = useRef(null);

	const handleInputChange = (title: TrackType['title']) => {
		// updateTrack(track.$loki, { title });
		setTackTitle(title);
	};

	useEffect(() => {
		// if (edit) inputRef.current.focus();
	}, [edit]);

	return (
		<View
			style={[
				GlobalStyles.row,
				GlobalStyles.alignCenter,
				GlobalStyles.justifyBetween,
				GlobalStyles.paddingXs,
				GlobalStyles.marginBottomSm,
				GlobalStyles.rounded,
				GlobalStyles.bgGrey,
			]}
		>
			<View style={[GlobalStyles.row, GlobalStyles.justifyBetween]}>
				<TouchableOpacity
					onPress={() => {}}
					style={[
						GlobalStyles.row,
						GlobalStyles.centered,
						GlobalStyles.bgWhite,
						GlobalStyles.rounded,
						GlobalStyles.paddingSm,
					]}
				>
					<MaterialCommunityIcons name="play" color={DefaultStyles.secondary} size={18} />
				</TouchableOpacity>
			</View>

			<View style={[GlobalStyles.row, GlobalStyles.alignCenter]}>
				<TouchableOpacity
					onPress={() => setEdit(true)}
					style={[GlobalStyles.row, GlobalStyles.w100, GlobalStyles.paddingRightXs]}
				>
					<Text>{edit}</Text>
					{edit && (
						<InputField
							customStyles={[GlobalStyles.heading, GlobalStyles.marginLeftXs]}
							placeholder={'Nombre de la pista'}
							handleChange={handleInputChange}
							onBlur={() => {
								setEdit(false);
							}}
							value={trackTitle}
							// customRef={inputRef}
						/>
					)}
				</TouchableOpacity>

				<View style={[GlobalStyles.rounded, GlobalStyles.bgPrimary, GlobalStyles.paddingSm]}>
					<MaterialCommunityIcons
						name={getIconName(track.category || 'riffs') as any}
						size={15}
						color={DefaultStyles.white}
					/>
				</View>
			</View>

			<Text style={[GlobalStyles.label, GlobalStyles.textGrey]}>{formatDuration(track.duration)}</Text>
		</View>
	);
};

export default TrackComponent;
