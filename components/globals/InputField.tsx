import React, { useEffect, useRef, useState } from 'react';
import { View, Text, TextInput } from 'react-native';

// STYLES
import { ComponentStyles, DefaultStyles, GlobalStyles } from '../../styles/index';

// TYPES
import { InputFieldType } from '../../types';

const InputField = ({
	id,
	inputType = 'text', // 'text' | 'number' | 'password'
	placeholder,
	label,
	value = '',
	background = false,
	handleChange,
	onBlur,
	error,
	errorTextStyle = 'danger', // revisar
	textColor = 'black',
	textStyle,
	textAlign = 'left',
	customStyles = [{}],
	focus = false,
	...props
}: InputFieldType) => {
	const [showPass] = useState(false);

	const getTextColor = (color: InputFieldType['textColor']) => {
		switch (color) {
			case 'white':
				return GlobalStyles.textWhite;
			case 'black':
				return GlobalStyles.textBlack;
			case 'grey':
				return GlobalStyles.textGrey;
			default:
				return GlobalStyles.textWhite;
		}
	};

	const getStyleWithBackground = () => {
		if (background) {
			return {borderColor: 'black', backgroundColor: (DefaultStyles as any)[background]}
		}else return ComponentStyles.inputStyleBackground;
	};

	// Mapping color and textColor to GlobalStyles
	const textColorStyle = getTextColor(textColor);

	const textFontStyle = GlobalStyles[textStyle || 'paragraph'];

	const inputStyle = {
		...textColorStyle,
		...textFontStyle,
		...getStyleWithBackground(),
	};

	return (
		<View style={[GlobalStyles.body, { margin: 10 },  ...customStyles]}>
			{label && <Text style={[GlobalStyles.label, textColorStyle, { textAlign }]}>{label}</Text>}

			<TextInput
				style={[ComponentStyles.inputStyle, inputStyle]}
				onChangeText={(value) => handleChange(value)}
				onBlur={() => onBlur && onBlur()}
				value={String(value)}
				placeholder={placeholder}
				secureTextEntry={inputType === 'password' && !showPass}
				keyboardType={inputType === 'number' ? 'numeric' : 'default'}
				autoFocus={focus}
				id={id}
				{...props}
			/>

			{error?.msg && <Text style={[GlobalStyles.paragraph, GlobalStyles.errorParagraph]}>{error.msg}</Text>}
		</View>
	);
};

export default InputField;
