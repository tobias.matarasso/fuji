import React from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

// ICONS
import { MaterialCommunityIcons } from '@expo/vector-icons';

// STYLES
import { GlobalStyles, ComponentStyles, DefaultStyles } from '../../styles';

// TYPES
import { ButtonType } from '../../types';
import { useNameFormatter } from '../../hooks';

const Button = ({
    title,
    action,
    loading,
    disabled = false,
    color = 'none',
    bold = false,
    icon,
    textStyle = 'subTitle',
    textColor = 'white',
    rounded = false,
    padding = true,// Nueva opción
}: ButtonType) => {
    const { capitalice } = useNameFormatter();

    const buttonContent = (
        <>
            {loading ? (
                <ActivityIndicator size="small" color={(DefaultStyles as any)[textColor]} />
            ) : (
                <View style={[ComponentStyles.viewStyle, color !== 'none' && GlobalStyles.paddingXXs]}>
                    {icon && (
                        <MaterialCommunityIcons name={icon as any} color={textColor} size={20} />
                    )}
                    <Text
                        style={[
                            GlobalStyles[textStyle],
                            GlobalStyles[`text${capitalice(textColor)}`],
                            icon ? GlobalStyles.marginLeftXs : {},
                            bold ? { fontWeight: 'bold' } : {},
                        ]}
                    >
                        {title}
                    </Text>
                </View>
            )}
        </>
    );

    return color == 'gradient' ? (
        <LinearGradient
            colors={[DefaultStyles.primary, DefaultStyles.secondary]} // Gradiente violeta a naranja
            start={{ x: 0, y: 1 }} // Abajo a la izquierda
            end={{ x: 1, y: 0 }} // Arriba a la derecha
            style={[
                ComponentStyles.buttonStyle,
                padding
                    ? padding === 'small'
                        ? [GlobalStyles.paddingXsm]
                        : [GlobalStyles.paddingXmd, GlobalStyles.paddingYxxs]
                    : {},
                rounded ? GlobalStyles.roundedCircle : GlobalStyles.rounded,
            ]}
        >
            <TouchableOpacity
                onPress={action}
                disabled={disabled || loading}
            >
                {buttonContent}
            </TouchableOpacity>
        </LinearGradient>
    ) : (
        <TouchableOpacity
            onPress={action}
            disabled={disabled || loading}
            style={[
                ComponentStyles.buttonStyle,
                padding
                    ? padding === 'small'
                        ? [GlobalStyles.paddingXsm]
                        : [GlobalStyles.paddingXmd, GlobalStyles.paddingYxxs]
                    : {},
                GlobalStyles[`bg${capitalice(color)}`],
                rounded ? GlobalStyles.roundedCircle : GlobalStyles.rounded,
            ]}
        >
            {buttonContent}
        </TouchableOpacity>
    );
};

export default Button;
