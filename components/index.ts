// GLOBALS
import Button from './globals/Button';
import InputField from './globals/InputField';

import SongComponent from './SongComponent';
import SectionComponent from './SectionComponent';
import LyricComponent from './LyricComponent';
import MicIcon from './MicIcon';
import RecorderButton from './RecorderButton';
import TrackComponent from './TrackComponent';

// MODALS
import { GeneralStructureModal, CreateSongComponent } from './modals';

export {
	// GENERALS
	Button,
	InputField,
	SongComponent,
	SectionComponent,
	LyricComponent,
	MicIcon,
	RecorderButton,
	TrackComponent,

	// MODALS
	GeneralStructureModal,
	CreateSongComponent,
};
