import Defaults from './Defaults';

const SpacingStyles = {
    paddingLg: {
        padding: Defaults.spacing_lg,
    },
    paddingMd: {
        padding: Defaults.spacing_md,
    },
    
    paddingSm: {
        padding: Defaults.spacing_sm,
    },
    
    paddingXs: {
        padding: Defaults.spacing_xs,
    },
    
    paddingXXs: {
        padding: Defaults.spacing_xxs,
    },
    
    paddingXLg: {
        paddingHorizontal: Defaults.spacing_lg,
    },
    
    paddingXmd: {
        paddingHorizontal: Defaults.spacing_md,
    },
    
    paddingXsm: {
        paddingHorizontal: Defaults.spacing_sm,
    },
    
    paddingXxs: {
        paddingHorizontal: Defaults.spacing_xs,
    },
    
    paddingYmd: {
        paddingVertical: Defaults.spacing_md,
    },
    
    paddingYsm: {
        paddingVertical: Defaults.spacing_sm,
    },
    
    paddingYxs: {
        paddingVertical: Defaults.spacing_xs,
    },
    
    paddingYxxs: {
        paddingVertical: Defaults.spacing_xxs,
    },
    
    paddingTopXl: {
        paddingTop: Defaults.spacing_xl,
    },
    
    paddingTopLg: {
        paddingTop: Defaults.spacing_lg,
    },
    
    paddingTopMd: {
        paddingTop: Defaults.spacing_md,
    },
    
    paddingTopSm: {
        paddingTop: Defaults.spacing_sm,
    },
    
    paddingTopXs: {
        paddingTop: Defaults.spacing_xs,
    },
    
    paddingBottomLg: {
        paddingBottom: Defaults.spacing_lg,
    },
    
    paddingBottomMd: {
        paddingBottom: Defaults.spacing_md,
    },
    
    paddingBottomSm: {
        paddingBottom: Defaults.spacing_sm,
    },
    
    paddingBottomXs: {
        paddingBottom: Defaults.spacing_xs,
    },
    
    paddingBottomXl: {
        paddingBottom: Defaults.spacing_xl,
    },
    
    paddingLeftMd: {
        paddingLeft: Defaults.spacing_md,
    },
    
    paddingLeftSm: {
        paddingLeft: Defaults.spacing_sm,
    },
    
    paddingLeftXs: {
        paddingLeft: Defaults.spacing_xs,
    },
    
    paddingRightMd: {
        paddingRight: Defaults.spacing_md,
    },
    
    paddingRightSm: {
        paddingRight: Defaults.spacing_sm,
    },
    
    paddingRightXs: {
        paddingRight: Defaults.spacing_xs,
    },

    // MARGIN
    marginMd: {
        margin: Defaults.spacing_md,
    },
    
    marginSm: {
        margin: Defaults.spacing_sm,
    },
    
    marginXs: {
        margin: Defaults.spacing_xs,
    },
    
    marginXmd: {
        marginHorizontal: Defaults.spacing_md,
    },
    
    marginXsm: {
        marginHorizontal: Defaults.spacing_sm,
    },
    
    marginXxs: {
        marginHorizontal: Defaults.spacing_xs,
    },
    
    marginYmd: {
        marginVertical: Defaults.spacing_md,
    },
    
    marginYsm: {
        marginVertical: Defaults.spacing_sm,
    },
    
    marginYxs: {
        marginVertical: Defaults.spacing_xs,
    },
    
    marginTopXlg: {
        marginTop: Defaults.spacing_xl,
    },
    
    marginTopMd: {
        marginTop: Defaults.spacing_md,
    },
    
    marginTopSm: {
        marginTop: Defaults.spacing_sm,
    },
    
    marginTopXs: {
        marginTop: Defaults.spacing_xs,
    },
    
    marginBottomXl: {
        marginBottom: Defaults.spacing_xl,
    },
    
    marginBottomLg: {
        marginBottom: Defaults.spacing_lg,
    },
    
    marginBottomMd: {
        marginBottom: Defaults.spacing_md,
    },
    
    marginBottomSm: {
        marginBottom: Defaults.spacing_sm,
    },
    
    marginBottomXs: {
        marginBottom: Defaults.spacing_xs,
    },
    
    marginLeftMd: {
        marginLeft: Defaults.spacing_md,
    },
    
    marginLeftSm: {
        marginLeft: Defaults.spacing_sm,
    },
    
    marginLeftXs: {
        marginLeft: Defaults.spacing_xs,
    },
    
    marginRightMd: {
        marginRight: Defaults.spacing_md,
    },
    
    marginRightSm: {
        marginRight: Defaults.spacing_sm,
    },
    
    marginRightXs: {
        marginRight: Defaults.spacing_xs,
    }
}

  export default SpacingStyles;
