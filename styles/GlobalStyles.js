import { StyleSheet } from 'react-native';
import SpacingStyles from './SpacingStyles';
import Defaults from './Defaults';

const GlobalStyles = StyleSheet.create({
	body: {
		fontFamily: Defaults.font_family,
		color: Defaults.black,
		backgroundColor: Defaults.grey,
	},

	container: {
		flex: 1,
		backgroundColor: Defaults.white
	},

	/* BACKGROUNDS */
	bgGradient: {
		backgroundColor: Defaults.gradient,
	},

	bgPrimary: {
		backgroundColor: Defaults.primary,
	},

	bgWhite: {
		backgroundColor: Defaults.white,
	},

	bgBlack: {
		backgroundColor: Defaults.black,
	},

	bgLightGrey: {
		backgroundColor: Defaults.lightGrey,
	},

	bgGrey: {
		backgroundColor: Defaults.grey,
	},

	bgSecondary: {
		backgroundColor: Defaults.secondary,
	},

	bgSuccess: {
		backgroundColor: Defaults.success,
	},

	bgFaded: {
		backgroundColor: Defaults.faded_bg,
	},

	bgDanger: {
		backgroundColor: Defaults.danger,
	},

	bgNone: {
		backgroundColor: 'transparent',
	},

	/* TEXTS TYPES */
	title: {
		fontWeight: 'bold',
		fontSize: Defaults.font_size_xl,
	},

	subTitleBold: {
		fontWeight: 'bold',
		fontSize: Defaults.font_size_lg,
	},

	subTitle: {
		fontSize: Defaults.font_size_lg,
	},

	heading: {
		fontSize: Defaults.font_size_md,
	},

	paragraph: {
		fontSize: Defaults.font_size_sm,
	},

	errorParagraph: {
		color: 'red',
	},

	label: {
		fontSize: Defaults.font_size_xs,
	},

	labelBold: {
		fontWeight: 'bold',
		fontSize: Defaults.font_size_xs,
	},

	labelXS: {
		fontSize: Defaults.font_size_xxs,
	},

	textCenter: {
		textAlign: 'center',
	},

	// TEXT COLORS
	textPrimary: {
		color: Defaults.primary,
	},

	textSecondary: {
		color: Defaults.secondary,
	},

	textSuccess: {
		color: Defaults.success,
	},

	textBlack: {
		color: Defaults.black,
	},

	textWhite: {
		color: Defaults.white,
	},

	textGrey: {
		color: Defaults.darkGrey,
	},

	textDanger: {
		color: Defaults.danger,
	},

	// BORDER RADIUS
	rounded: {
		borderRadius: Defaults.rounded,
	},
	roundedSm: {
		borderRadius: Defaults.rounded_sm,
	},
	roundedCircle: {
		borderRadius: Defaults.rounded_circle,
	},

	// SPACING
	...SpacingStyles,

	// POSITION
	absolute: {
		position: 'absolute',
	},

	// DISPLAY
	row: {
		flexDirection: 'row',
	},

	// ALIGMENT
	alignCenter: {
		alignItems: 'center',
	},

	justifyBetween: {
		justifyContent: 'space-between',
	},

	justifyAround: {
		justifyContent: 'space-around',
	},

	centered: {
		justifyContent: 'center',
		alignItems: 'center'
	},

	// SHADOW
	shadow: {
		shadowColor: '#6E6E6E',
		shadowOffset: {
			width: 0,
			height: 3,
		},
		shadowOpacity: 0.30,
		shadowRadius: 8,
		elevation: 4, // This property is for Android shadow
	},

	w100: {
		width: '100%',
	},

	// BUDGET
	badge: {
		position: 'absolute',
		left: 14,
		top: 12,
		backgroundColor: Defaults.grey,
		borderRadius: 9,
		width: 12,
		height: 12,
		justifyContent: 'center',
		alignItems: 'center',
	},

	burgetCount: {
		color: Defaults.black,
		fontSize: 10,
		fontWeight: 'bold',
	},

});

export default GlobalStyles;
