const DefaultStyles = {
	// COLORS
	primary: '#C594D1',
	secondary: '#FFC49A',
	danger: '#DF9595',
	black: '#444444',
	grey: '#EEEEEE',
	white: '#FFFFFF',
	background: '#FCFCFC',
	footerBg: '#FFFFFF',
	darkGrey: '#A1A1A1',
	lightGrey: '#f5f5f5',
	faded_bg: '#00000033',

	// FONTS
	font_family: 'Inter, sans-serif',
	font_size_xl: 25,
	font_size_lg: 18,
	font_size_md: 16,
	font_size_sm: 14,
	font_size_xs: 12,
	font_size_xxs: 10,

	// SPACING
	spacing_xl: 40,
	spacing_lg: 30,
	spacing_md: 20,
	spacing_sm: 15,
	spacing_xs: 10,
	spacing_xxs: 5,

	// BORDER RADIUS
	rounded: 12,
	rounded_sm: 8,
	rounded_circle: 100
};

export default DefaultStyles;
