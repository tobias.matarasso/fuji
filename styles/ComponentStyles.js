import { StyleSheet } from 'react-native'
import DefaultStyles from './Defaults'

const ComponentStyles = StyleSheet.create({
    // Button styles
    buttonStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        borderRadius: 8
    },
    viewStyle: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },

    // Text input
    inputStyle: {
        paddingHorizontal: 5
    },
    
    inputStyleBackground: {
        backgroundColor: 'white',
        borderRadius: 8
    }
})

export default ComponentStyles
