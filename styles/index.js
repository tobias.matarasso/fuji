import DefaultStyles from './Defaults'
import GlobalStyles from './GlobalStyles'
import ComponentStyles from './ComponentStyles'

export { DefaultStyles, GlobalStyles, ComponentStyles }
