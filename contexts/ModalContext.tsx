import React from 'react';
import { createContext, useState } from 'react';
import { ModalContextType } from '../types';

export const ModalContext = createContext<ModalContextType | undefined>(undefined);

export const ModalProvider = ({ children }) => {
	const [modalVisible, setModalVisible] = useState<React.JSX.Element | false>(false);
	const [needRefresh, setNeedRefresh] = useState<boolean>(false);
	const [dataToRefresh, setDataToRefresh] = useState<any>();

	return (
		<ModalContext.Provider
			value={{
				modalVisible,
				setModalVisible,
				needRefresh,
				setNeedRefresh,
				dataToRefresh,
				setDataToRefresh,
			}}
		>
			{children}
		</ModalContext.Provider>
	);
};
