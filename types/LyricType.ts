export interface LyricType {
	$loki?: number;
	title?: string;
	lyrics?: string;
	created_at?: number;
	songId?: number | false;
}