import { Dispatch, SetStateAction } from 'react';

export interface ModalContextType {
	modalVisible: React.JSX.Element | false;
	setModalVisible: Dispatch<SetStateAction<React.JSX.Element | false>>;
	needRefresh: boolean;
	setNeedRefresh: Dispatch<SetStateAction<boolean>>;
	dataToRefresh: any;
	setDataToRefresh: Dispatch<SetStateAction<any>>;
}
