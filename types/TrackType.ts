export interface TrackType {
	$loki?: number;
	title: string;
	duration: number;
	uri: string;
	created_at: number;
	songId: number | false;
	category: CategoriesType | false;
}

export interface ListOfCategoriesType {
	name: CategoriesNamesType;
	icon?: string;
	category?: CategoriesType;
	materialIcons?: boolean;
	fontAwesome?: boolean;
}

export type CategoriesNamesType = 'Riffs' | 'Canciones' | 'Ideas' | 'Notas de voz' | 'Ritmos' | 'Lineas de bajo';

export type CategoriesType =
	| 'riffs'
	| 'canciones'
	| 'ideas'
	| 'notas_de_voz'
	| 'ritmos'
	| 'lineas_de_bajo'
	| 'unlinked';

export interface UpdateTrackType {
	title?: TrackType['title'] | undefined;
	duration?: TrackType['duration'] | undefined;
	songId?: TrackType['songId'] | undefined;
	category?: TrackType['category'] | undefined;
}
