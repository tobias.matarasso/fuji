export interface SongType {
  $loki?: number;
  title: string;
  created_at: number;
  fav: boolean;
  last_updated: number;
  tracks: number;
  lyrics: number;
  sections: SectionType[];
}

export interface SectionType {
  sectionId: number;
  title: string;
  tracks: number;
  created_at: number;
}