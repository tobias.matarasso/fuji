import { ColorsType } from "./GeneralTypes";

export interface ButtonType {
    title: string;
    action?: () => void;
    disabled?: boolean;
    loading?: boolean;
    color?: ButtonColorsType | 'gradient';
    bold?: boolean;
    icon?: string;
    rounded?: boolean;
    outlined?: boolean;
    textStyle?: 'title' | 'subTitle' | 'heading' | 'paragraph' | 'label';
    textColor?: 'primary' | 'secondary' | 'black' | 'success' | 'danger' | 'white' | 'grey' ;
    customStyles?: any;
    iconPosition?: 'before' | 'after';
    imageSource?: any;
    size?: 'regular' | 'small';
    padding?: boolean | 'small';
};

export type ButtonColorsType = 'primary' | 'secondary' | 'black' | 'success' | 'danger' | 'white' | 'none' | string;

export interface InputFieldType {
	id?: string;
	inputType?: 'text' | 'number' | 'email' | 'password' | 'date' | 'select';
	placeholder?: string;
	label?: string;
	value?: string | number | undefined;
	required?: boolean;
    background?: false | ColorsType;
    error?: {msg: string};
	errorTextStyle?: 'danger' | 'primary';
	textStyle?: 'title' | 'subTitle' | 'subTitleBold' | 'heading' | 'paragraph' | 'label';
	textAlign?: 'left' | 'center' | 'right';
	textColor?: 'black' | 'white' | 'grey';
	handleChange: (e: string) => void;
	onBlur?: () => void;
	disabled?: boolean;
	focus?: boolean;
	customStyles?: any;
	maxLength?: number;
	loading?: boolean;

};