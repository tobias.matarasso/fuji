import Navigation from './navigation';

// CONTEXTS
import { ModalProvider } from './contexts';

export default function App() {

  return (
    <ModalProvider>
      <Navigation />
    </ModalProvider>
  );
}