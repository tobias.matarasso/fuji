import Loki from 'lokijs';
import AsyncStorage from '@react-native-async-storage/async-storage';

class LokiAsyncStorageAdapter {
  async loadDatabase(dbname: string, callback: (data: any) => void) {
    try {
      const data = await AsyncStorage.getItem(dbname);
      callback(data);
    } catch (error) {
      callback(new Error('Failed to load database'));
    }
  }

  async saveDatabase(dbname: string, dbstring: string, callback: (error?: Error) => void) {
    try {
      await AsyncStorage.setItem(dbname, dbstring);
      callback();
    } catch (error) {
      callback(new Error('Failed to save database'));
    }
  }

  async deleteDatabase(dbname: string, callback: (error?: Error) => void) {
    try {
      await AsyncStorage.removeItem(dbname);
      callback();
    } catch (error) {
      callback(new Error('Failed to delete database'));
    }
  }
}

const adapter = new LokiAsyncStorageAdapter();

const db = new Loki('fuji.db', {
  adapter,
  autoload: true,
  autoloadCallback: databaseInitialize,
  autosave: true,
  autosaveInterval: 4000,
});

function databaseInitialize() {
  const collections = ['tracks', 'songs', 'lyrics']; // Add your collection names here
  collections.forEach((collectionName) => {
    let collection = db.getCollection(collectionName);
    if (collection === null) {
      collection = db.addCollection(collectionName);
    }
  });
}

export default db;