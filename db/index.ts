import db from "./db";

// MODELS
import { Lyric } from "./models/LyricModel";
import { Song, Section } from "./models/SongModel";

export { db, Song, Lyric, Section };