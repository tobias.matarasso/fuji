import { SectionType, SongType } from '../../types';

export class Song implements SongType {
	title: string;
	created_at: number;
	fav: boolean;
	last_updated: number;
	tracks: number;
	lyrics: number;
	sections: SectionType[];

	constructor(title: string) {
		this.title = title;
		this.created_at = new Date().getTime();
		this.fav = false;
		this.last_updated = new Date().getTime();
		this.tracks = 0;
		this.lyrics = 0;
		this.sections = [];
	}
}

export class Section implements SectionType {
	sectionId: number;
	title: string;
	tracks: number;
	created_at: number;

	constructor(sectionId: number, title: string) {
		this.sectionId = sectionId;
		this.title = title;
		this.tracks = 0;
		this.created_at = new Date().getTime();
	}
}
