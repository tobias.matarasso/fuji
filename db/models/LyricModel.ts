import { LyricType } from '../../types';

export class Lyric implements LyricType {
	title: string;
	lyrics: string;
	created_at: number;
	songId: number | false;

	constructor({title = '', lyrics = '', songId = false}: LyricType) {
		this.title = title;
		this.created_at = new Date().getTime();
		this.lyrics = lyrics;
		this.songId = songId;
	}
}
