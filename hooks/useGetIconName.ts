import React from 'react';

// TYPES
import { CategoriesType } from '../types';

const useGetIconName = () => {
	const getIconName = (category: CategoriesType) => {
		switch (category) {
			case 'riffs':
				return 'music-note';
			case 'canciones':
				return 'microphone-variant';
			case 'ideas':
				return 'lightbulb';
			case 'notas_de_voz':
				return 'record-voice-over';
			case 'ritmos':
				return 'drum';
			case 'lineas_de_bajo':
				return 'violin';
			case 'unlinked':
				return 'delete-empty';
			default: 
				return 'music-note'
		}
	};

	return { getIconName };
};

export default useGetIconName;
