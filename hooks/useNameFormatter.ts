const useNameFormatter = () => {	

	const formatName = (name: string, lastName: string) =>
		`${capitalice(name)}${lastName ? ` ${capitalice(lastName)}` : ''}`;

	const capitalice = (str: string) => (str ? `${str.charAt(0).toUpperCase()}${str.slice(1)}` : '');

	const formatDuration = (time: number) => {
		const minutes = time / 1000 / 60;
		const minutesDisplay = Math.floor(minutes);
		const seconds = Math.round((minutes - minutesDisplay) * 60);
		const secondsDisplay = seconds < 10 ? `0${seconds}` : seconds;
		return `${minutesDisplay}:${secondsDisplay}`;
	};

	return { formatName, capitalice, formatDuration };
};

export default useNameFormatter;
