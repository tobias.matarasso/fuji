import { useEffect, useState, useCallback } from 'react';
import { db } from '../db';

interface UseLokiCollectionOptions {
	sortBy?: string;
	sortDesc?: boolean;
	limit?: number;
	query?: {} | null;
}

export function useLokiCollection<T>() {
	const loadData = (
		collectionName: string,
		{ sortBy = 'created_at', sortDesc = true, limit, query = null }: UseLokiCollectionOptions = {}
	) => {
		const collection = db.getCollection(collectionName);
		if (collection) {
			let chain = collection.chain();

			// Aplicar la consulta solo si no está vacía
			if (query && Object.keys(query).length > 0) {
				chain = chain.find(query);
			}

			const latestItems = chain
				.simplesort(sortBy, sortDesc) // Ordenar por created_at en orden descendente
				.limit(limit)
				.data();
			return latestItems;
		}
	};

	const insertOne = async (collectionName: string, item: any) => {
		const collection = db.getCollection(collectionName);
		// TODO: Ejecuta el save pero es asincrono y no se como hacer que espere
		const newItem = collection.insert(item);

		await new Promise<void>((resolve, reject) => {
			db.saveDatabase((err: any) => {
				if (err) {
					console.error('Error al guardar la base de datos:', err);
					reject(err);
				} else {
					resolve(newItem);
				}
			});
		});

		return newItem;
	};

	const updateOne = async (collectionName: string, item: any) => {

		const collection = db.getCollection(collectionName);
		
		try {
			await collection.update(item);
		} catch (error) {
			// A veces falla la actualización, no se por qué, pero si no pongo esto, no no sigue el proceso asincrono
			console.log('error', item);
		}

		await new Promise<void>((resolve, reject) => {
			db.saveDatabase((err: any) => {
				if (err) {
					console.error('Error al guardar la base de datos:', err);
					reject(err);
				} else {
					resolve();
				}
			});
		});

		return;
	};

	const deleteOne = async (collectionName: string, id: number) => {
		const collection = db.getCollection(collectionName);
		collection.removeWhere((item: any) => item.$loki === id);

		await new Promise<void>((resolve, reject) => {
			db.saveDatabase((err: any) => {
				if (err) {
					console.error('Error al guardar la base de datos:', err);
					reject(err);
				} else {
					resolve();
				}
			});
		});

		return;
	};

	return { loadData, insertOne, updateOne, deleteOne };
}
