import React, { useContext, useState } from 'react';
import { View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

// ICONS
import { MaterialCommunityIcons, MaterialIcons } from '@expo/vector-icons';

// CONTEXTS
// import { ModalContext } from './contexts';

// COMPONENTS
import { GeneralStructureModal, MicIcon } from './components';

// SCREENS
import { HomeScreen, SongsScreen, CategoryScreen, LyricScreen, CategoryView, SongView, LyricView } from './screens';

// STYLES
import { DefaultStyles, GlobalStyles } from './styles/index';
// import { SaveRecordView } from './screens';

// TAB
const Tab = createBottomTabNavigator();

// STAKS
const Stack = createStackNavigator();

const Navigation = () => {
	const backHeaderOptions = {
		headerShown: true,
		headerTitle: '',
		headerBackTitleVisible: false, // Agrega esta línea
		headerStyle: {
			borderBottomWidth: 0,
			shadowOpacity: 0,
		},
	};

	const EmptyComponent = () => {
		return <View />;
	};

	function Home() {
		return (
			<Tab.Navigator
				initialRouteName="HomeView"
				screenOptions={{
					tabBarActiveTintColor: DefaultStyles.black,
					tabBarInactiveTintColor: DefaultStyles.darkGrey,
					cardStyle: { backgroundColor: DefaultStyles.white },
					headerShown: true, // Asegúrate de que se muestre la cabecera
					headerRight: () => (
						<View
							style={[
								GlobalStyles.roundedCircle,
								GlobalStyles.bgGrey,
								GlobalStyles.marginRightMd,
								GlobalStyles.paddingYxs,
								{ paddingHorizontal: 10 },
							]}
						>
							<MaterialIcons name="cloud-off" size={18} color={DefaultStyles.darkGrey} />
						</View>
					),
					headerTitle: '',
					headerStyle: {
						borderBottomWidth: 0,
						shadowOpacity: 0,
					},
				}}
			>
				<Tab.Screen
					name="HomeView"
					component={HomeScreen}
					options={{
						tabBarLabel: 'Home',
						tabBarIcon: ({ color, size }) => (
							<MaterialCommunityIcons name="home" color={color} size={size} />
						),
					}}
				/>

				<Tab.Screen
					name="Songs"
					component={SongsScreen}
					options={{
						tabBarLabel: 'Songs',
						tabBarIcon: ({ color, size }) => (
							<MaterialIcons name="my-library-music" color={color} size={size} />
						),
					}}
				/>

				<Tab.Screen
					name="Mic"
					component={EmptyComponent}
					listeners={{
						tabPress: (e) => {
							e.preventDefault();
						},
					}}
					options={{
						tabBarLabel: '',
						tabBarIcon: () => (
							<View
								style={[
									GlobalStyles.roundedCircle,
									GlobalStyles.bgWhite,
									GlobalStyles.paddingSm,
									GlobalStyles.absolute,
									GlobalStyles.shadow,
								]}
							>
								<MicIcon />
							</View>
						),
						headerShown: false,
					}}
				/>

				<Tab.Screen
					name="Cats"
					component={CategoryScreen}
					options={{
						tabBarLabel: 'Cats',
						tabBarIcon: ({ color, size }) => (
							<MaterialCommunityIcons name="view-grid" color={color} size={size} />
						),
					}}
				/>

				<Tab.Screen
					name="Lyrics"
					component={LyricScreen}
					options={{
						tabBarLabel: 'Lyrics',
						tabBarIcon: ({ color, size }) => (
							<MaterialIcons name="speaker-notes" color={color} size={size} />
						),
					}}
				/>
			</Tab.Navigator>
		);
	}

	return (
		<NavigationContainer>
			<Stack.Navigator
				initialRouteName="Home"
				screenOptions={{
					headerShown: false, // Oculta el encabezado para todas las pantallas
				}}
			>
				<Stack.Screen name="Home" component={Home} />
				{/* <Stack.Screen name="SaveRecord" component={SaveRecordView} options={backHeaderOptions} /> */}
				<Stack.Screen name="CategoryView" component={CategoryView} options={backHeaderOptions} />
				<Stack.Screen name="SongView" component={SongView} options={backHeaderOptions} />
				<Stack.Screen name="LyricView" component={LyricView} options={backHeaderOptions} />
			</Stack.Navigator>

			<GeneralStructureModal />
		</NavigationContainer>
	);
};

export default Navigation;
